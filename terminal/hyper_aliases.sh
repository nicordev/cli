alias please_hyper_how_to_install_solarized_theme='echo "
hyper i hyper-solarized-light
"'
alias please_hyper_where_is_configuration_file='echo "
~/Library/Application Support/Hyper/.hyper.js
"'
alias please_hyper_how_to_add_transparency_plugin='echo "
hyper i hyper-opacity
"'
