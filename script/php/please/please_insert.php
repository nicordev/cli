#! /usr/bin/env php
<?php

define('SCRIPT_NAME', pathinfo(array_shift($argv), PATHINFO_BASENAME));
define('FUNCTION_NAME', array_shift($argv));

class EditFile
{
    private const int DEFAULT_USE_LINE = 1;

    private array $lines = [];
    private string $file;

    public function read_file(string $file): void
    {
        $this->file = $file;
        $this->lines = explode("\n", file_get_contents($file));
    }

    public function insert_use_function_statement(string $function_with_namespace): void
    {
        $offset = $this->find_namespace_line() + 1 ?? self::DEFAULT_USE_LINE;

        array_splice(
            array: $this->lines,
            offset: $offset,
            length: 0,
            replacement: "use function {$function_with_namespace};",
        );
    }

    public function write_file(): void
    {
        file_put_contents(filename: $this->file, data: implode("\n", $this->lines));
    }

    private function find_namespace_line(): ?int
    {
        foreach ($this->lines as $i => $line) {
            if (preg_match('#namespace \w+#', $line)) {
                return $i;
            }
        }

        return null;
    }
}

function use_function_statement(array $arguments) {
    if (count($arguments) < 2) {
        echo sprintf("%s %s\033[33m function_with_namespace file [file]\033[0m\n", SCRIPT_NAME, FUNCTION_NAME);

        return;
    }

    $editFile = new EditFile();
    $function = array_shift($arguments);

    foreach ($arguments as $file) {
        $editFile->read_file($file);
        $editFile->insert_use_function_statement("$function");
        $editFile->write_file();
    }
}

function howItWorks() {
    echo _getScriptContent();
}

function _getScriptContent() {
    return file_get_contents(__DIR__.'/'.SCRIPT_NAME);
}

function _listFunctions() {
    $content = _getScriptContent();
    $functions = [];
    preg_match_all('#^function ([a-z]{1}[a-zA-Z0-9_]*)\(#m', $content, $functions);
    asort($functions[1]);
    echo implode("\n", $functions[1])."\n";
}

if ($argc <= 1) {
    _listFunctions();
    exit;
}

call_user_func(FUNCTION_NAME, $argv);
