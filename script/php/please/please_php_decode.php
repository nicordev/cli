#! /usr/bin/env php
<?php

define('SCRIPT_NAME', pathinfo(array_shift($argv), PATHINFO_BASENAME));
define('FUNCTION_NAME', array_shift($argv));

function _exportVariable($variable, $indent="") {
    $variableType = gettype($variable);

    if ('string' === $variableType) {

        return '"' . addcslashes($variable, "\\\$\"\r\n\t\v\f") . '"';
    }

    if ('array' === $variableType) {
        if ([] === $variable) {

            return "[]";
        }

        $resultParts = [];
        $isIndexedArray = array_keys($variable) === range(0, count($variable) - 1);

        foreach ($variable as $key => $value) {
            $partKey = ($isIndexedArray ? "" : _exportVariable($key) . " => ");
            $partValue = _exportVariable($value, "$indent    ");
            $resultParts[] = "$indent    {$partKey}{$partValue}";
        }

        $result = implode(",\n", $resultParts);

        return "[\n{$result}\n{$indent}]";
    }

    if ('boolean' === $variableType) {
        return $variable ? "true" : "false";
    }

    return var_export($variable, true);
}

function _printExportVariable(string $jsonContent) {
    try {
        $decodedContent = json_decode($jsonContent, true, 512, JSON_THROW_ON_ERROR);
    } catch (Exception $exception) {
        echo $jsonContent . "\n";

        exit(1);
    }

    echo _exportVariable($decodedContent) . "\n";
}

function jsonFileToPhp(array $arguments) {
    if (count($arguments) < 1) {
        echo sprintf("%s %s\033[33m jsonFile\033[0m\n", SCRIPT_NAME, FUNCTION_NAME);

        return;
    }

    $jsonFile = $arguments[0];
    $jsonContent = file_get_contents($jsonFile);

    _printExportVariable($jsonContent);
}

function jsonToPhp(array $arguments) {
    if (count($arguments) >= 1) {
        echo sprintf("myCommand | %s %s\n", SCRIPT_NAME, FUNCTION_NAME);

        return;
    }

    $jsonContent = '';

    while (false !== ($line = fgets(STDIN))) {
        $jsonContent .= $line;
    }

    _printExportVariable($jsonContent);
}

function howItWorks() {
    echo _getScriptContent();
}

function _getScriptContent() {
    return file_get_contents(__DIR__.'/'.SCRIPT_NAME);
}

function _listFunctions() {
    $content = _getScriptContent();
    $functions = [];
    preg_match_all('#^function ([a-z]{1}[a-zA-Z0-9]*)\(#m', $content, $functions);
    asort($functions[1]);
    echo implode("\n", $functions[1])."\n";
}

if ($argc <= 1) {
    _listFunctions();
    exit;
}

call_user_func(FUNCTION_NAME, $argv);
