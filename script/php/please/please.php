#! /usr/bin/env php
<?php

define('SCRIPT_NAME', pathinfo(array_shift($argv), PATHINFO_BASENAME));
define('FUNCTION_NAME', array_shift($argv));

function say_hello(array $arguments)
{
    if (count($arguments) < 1) {
        echo sprintf(<<< MESSAGE

            %s %s\033[33m name\033[0m
            \n
            MESSAGE,
            SCRIPT_NAME,
            FUNCTION_NAME,
        );

        return;
    }

    $name = $arguments[0];

    echo "Hello {$name}!\n";
}

function how_it_works()
{
    echo _get_script_content();
}

function _get_script_content()
{
    return file_get_contents(__DIR__.'/'.SCRIPT_NAME);
}

function _list_actions()
{
    $content = _get_script_content();
    $matches = [];
    preg_match_all('#^function ([a-z]{1}[a-zA-Z0-9_]*)\(#m', $content, $matches);
    $actions = $matches[1];
    asort($actions);

    return $actions;
}

if ($argc <= 1) {
    echo implode("\n", _list_actions())."\n";
    exit;
}

$actions = _list_actions();

if (!in_array(FUNCTION_NAME, $actions)) {
    echo "Unknown action ".FUNCTION_NAME."\n";
    exit;
}

call_user_func(FUNCTION_NAME, $argv);
