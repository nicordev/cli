#! /usr/bin/env php
<?php

define('SCRIPT_NAME', pathinfo(array_shift($argv), PATHINFO_BASENAME));
define('FUNCTION_NAME', array_shift($argv));

function rename_files(array $arguments) {
    if (count($arguments) < 3) {
        echo sprintf("%s %s\033[33m search replace file [file]\n", SCRIPT_NAME, FUNCTION_NAME);

        return;
    }

    $search = array_shift($arguments);
    $replace = array_shift($arguments);

    foreach ($arguments as $file) {
        $newFile = str_replace($search, $replace, $file);
        rename($file, $newFile);

        echo "file $file renamed to $newFile\n";
    }
}

function rename_files_regex(array $arguments) {
    if (count($arguments) < 3) {
        echo sprintf("%s %s\033[33m search_regex replace file [file]\n", SCRIPT_NAME, FUNCTION_NAME);

        return;
    }

    $search = array_shift($arguments);
    $replace = array_shift($arguments);

    foreach ($arguments as $file) {
        $newFile = preg_replace($search, $replace, $file);
        rename($file, $newFile);

        echo "file $file renamed to $newFile\n";
    }
}

function how_it_works() {
    echo _getScriptContent();
}

function _getScriptContent() {
    return file_get_contents(__DIR__.'/'.SCRIPT_NAME);
}

function _list_functions() {
    $content = _getScriptContent();
    $functions = [];
    preg_match_all('#^function ([a-z]{1}[a-zA-Z0-9_]*)\(#m', $content, $functions);
    asort($functions[1]);
    echo implode("\n", $functions[1])."\n";
}

if ($argc <= 1) {
    _list_functions();
    exit;
}

call_user_func(FUNCTION_NAME, $argv);
