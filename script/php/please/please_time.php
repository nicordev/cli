#! /usr/bin/env php
<?php

define('SCRIPT_NAME', array_shift($argv));
define('FUNCTION_NAME', array_shift($argv));

class DateIntervalEnhanced extends DateInterval
{
    public function recalculate()
    {
        $from = new DateTime;
        $to = clone $from;
        $to = $to->add($this);
        $diff = $from->diff($to);

        foreach ($diff as $key => $value) {
            $this->$key = $value;
        }

        return $this;
    }
}

function convertTime($parameters = [])
{
    if (2 > count($parameters)) {
        echo sprintf(
            "%s %s \e[33mtime unit\e[0m\n\nunit:\n- hour: h\n- minute: m\n- second: s\n", 
            SCRIPT_NAME, 
            FUNCTION_NAME
        );

        return;
    }

    $time = sprintf("PT%s%s", $parameters[0], strtoupper($parameters[1]));
    $formattedTime = (new DateIntervalEnhanced($time))->recalculate()->format("%h:%i:%s");

    echo sprintf("\n%s\n", $formattedTime);
}

class TimeReader
{
    public static function read(string $time): DateTimeInterface
    {
        $parts = explode(':', $time);
        $minutes = $parts[1] ?? exit('Missing minutes. Use format hh:mm');
        $hours = $parts[0];

        return (new DateTimeImmutable())->setTime(hour: $hours, minute: $minutes);
    }
}

function diff($parameters = [])
{
    if (2 > count($parameters)) {
        echo sprintf(
            "%s %s \e[33mstart_at end_at\e[0m\n", 
            SCRIPT_NAME, 
            FUNCTION_NAME
        );

        return;
    }

    $start_at = TimeReader::read($parameters[0]);
    $end_at_at = TimeReader::read($parameters[1]);
    $result = $end_at_at->diff($start_at);

    echo $result->format('%H:%I')."\n";
}

function howItWorks()
{
    echo _getScriptContent();
}

function _getScriptContent()
{
    return file_get_contents(__DIR__ . '/' . SCRIPT_NAME);
}

function _listFunctions()
{
    $content = _getScriptContent();
    $functions = [];
    preg_match_all('#^function ([a-z]{1}[a-zA-Z0-9]*)\(#m', $content, $functions);
    echo implode("\n", $functions[1]) . "\n";
}

if ($argc <= 1) {
    _listFunctions();
    exit;
}

call_user_func(FUNCTION_NAME, $argv);
