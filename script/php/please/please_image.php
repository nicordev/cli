#! /usr/bin/env php
<?php

define('SCRIPT_NAME', pathinfo(array_shift($argv), PATHINFO_BASENAME));
define('FUNCTION_NAME', array_shift($argv));

function resize(array $arguments)
{
    $argumentsCount = count($arguments);

    if ($argumentsCount >= 1 && $argumentsCount < 3) {
        $originalImageSize = getimagesize($arguments[0]);
        $imageRatio = $originalImageSize[1] / $originalImageSize[0];
        echo "\n\033[1m{$arguments[0]}\033[0m\nwidth: {$originalImageSize[0]}\nheight: {$originalImageSize[1]}\nratio: $imageRatio\n";
    }

    if ($argumentsCount >= 2 && $argumentsCount < 3) {
        $widthOrHeight = (int) $arguments[1];
        $deducedDimension = (int) ($widthOrHeight * $imageRatio);
        echo "\nimage ratio: $imageRatio\ndeduced dimension: $deducedDimension\n";
    }

    if ($argumentsCount < 3) {
        echo sprintf("\n%s %s\033[33m imageFile width height [newImageFile]\n", SCRIPT_NAME, FUNCTION_NAME);

        return;
    }

    [$originalFile, $width, $height] = $arguments;
    $originalFileExtension = strtolower(pathinfo($originalFile, PATHINFO_EXTENSION));
    $originalFileName = pathinfo($originalFile, PATHINFO_FILENAME);
    $originalFilePath = pathinfo($originalFile, PATHINFO_DIRNAME);
    $extension = $originalFileExtension;

    if ('jpg' === $originalFileExtension) {
        $extension = 'jpeg';
    }

    if (false === function_exists("imagecreatefrom$extension") && false === function_exists("image$extension")) {
        throw new RuntimeException("can not handle $originalFileExtension files");
    }

    $newImageFile = $arguments[3] ?? "$originalFilePath/$originalFileName-resized.$originalFileExtension";
    [$originalWidth, $originalHeight] = getimagesize($originalFile);
    $original = "imagecreatefrom$extension"($originalFile);
    $resized = imagecreatetruecolor($width, $height);
    imagecopyresampled($resized, $original, 0, 0, 0, 0, $width, $height, $originalWidth, $originalHeight);
    "image$extension"($resized, $newImageFile);

    echo "\033[32m$newImageFile created.\n\033[34mHave a nice day! :)\033[0m\n";
}

function convertToPng(array $arguments)
{
    if (count($arguments) < 1) {
        echo sprintf("%s %s\033[33m imageFile\n", SCRIPT_NAME, FUNCTION_NAME);

        return;
    }

    [$originalFile] = $arguments;
    $originalFileExtension = pathinfo($originalFile, PATHINFO_EXTENSION);
    $originalFileName = pathinfo($originalFile, PATHINFO_FILENAME);
    $originalFilePath = pathinfo($originalFile, PATHINFO_DIRNAME);
    $pngFile = "$originalFilePath/$originalFileName.png";
    $extension = $originalFileExtension;

    if ('jpg' === $originalFileExtension) {
        $extension = 'jpeg';
    }

    if (false === function_exists("imagecreatefrom$extension")) {
        throw new RuntimeException("can not handle $extension files");
    }

    $original = "imagecreatefrom$extension"($originalFile);
    imagepng($original, $pngFile);
}

function howItWorks()
{
    echo _getScriptContent();
}

function _getScriptContent()
{
    return file_get_contents(__DIR__ . '/' . SCRIPT_NAME);
}

function _listFunctions()
{
    $content = _getScriptContent();
    $matches = [];
    preg_match_all('#^function ([a-z]{1}[a-zA-Z0-9]*)\(#m', $content, $matches);
    $functions = $matches[1];
    sort($functions);
    echo implode("\n", $functions) . "\n";
}

if ($argc <= 1) {
    _listFunctions();
    exit;
}

call_user_func(FUNCTION_NAME, $argv);
