#! /bin/bash

readonly SCRIPT_NAME=$(basename $BASH_SOURCE)
readonly SCRIPT_DIRECTORY=$(dirname $BASH_SOURCE)
readonly EXIT_CODE_BYE=187
readonly DNS_RESOLVER_FILE='/etc/resolv.conf'

setDnsToCloudFlare() {
    sed -Ei -e 's#\.[0-9]+#.1#g' -e 's#[0-9]+\.#1.#g' $DNS_RESOLVER_FILE
}

showDnsResolverFile() {
    cat $DNS_RESOLVER_FILE
    echo "
$DNS_RESOLVER_FILE
"
}

_handleExit() {
    if [ $? == $EXIT_CODE_BYE ]; then
        echo "Have a nice day!"
    fi
}

# Display the source code of this file
howItWorks() {
    if [ $# -lt 1 ]; then
        less "$0"

        return
    fi

    less --pattern="$@" "$0"
}

# List all functions that do not begin with an underscore _
_listAvailableFunctions() {
    cat $0 | grep -E '^[a-z]+[a-zA-Z0-9_]*\(\) \{$' | sed 's#() {$##' | sort
}

if [ $# -eq 0 ]; then
    _listAvailableFunctions
    exit
fi

trap _handleExit exit err

"$@"
