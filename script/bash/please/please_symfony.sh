#! /usr/bin/env bash

readonly SCRIPT_NAME=$(basename $0)
readonly SCRIPT_DIRECTORY=$(dirname $0)

# style
readonly STYLE_RESET=$(tput sgr0)
readonly STYLE_BOLD=$(tput bold)

# foreground
readonly FG_BLACK=$(tput setaf 0)
readonly FG_RED=$(tput setaf 1)
readonly FG_GREEN=$(tput setaf 2)
readonly FG_YELLOW=$(tput setaf 3)
readonly FG_BLUE=$(tput setaf 4)
readonly FG_MAGENTA=$(tput setaf 5)
readonly FG_CYAN=$(tput setaf 6)
readonly FG_WHITE=$(tput setaf 7)

# background
readonly BG_BLACK=$(tput setab 0)
readonly BG_RED=$(tput setab 1)
readonly BG_GREEN=$(tput setab 2)
readonly BG_YELLOW=$(tput setab 3)
readonly BG_BLUE=$(tput setab 4)
readonly BG_MAGENTA=$(tput setab 5)
readonly BG_CYAN=$(tput setab 6)
readonly BG_WHITE=$(tput setab 7)

readonly PHP_BIN_CONSOLE='php -d memory_limit=4G bin/console'

listValidationConstraints() {
    ls vendor/symfony/validator/Constraints
}

debugRouter() {
    if [ $# -lt 1 ]
    then
        printf "
${SCRIPT_NAME} ${FUNCNAME[0]} ${FG_YELLOW}routeHint${STYLE_RESET}
"

        return 1
    fi

    $PHP_BIN_CONSOLE debug:router | grep "$@"
}

debugEnvironmentVariables() {
    $PHP_BIN_CONSOLE debug:dotenv
}

# Display the source code of this file
howItWorks() {
    if [ $# -lt 1 ]
    then
        less "$0"

        return
    fi

    less --pattern="$@" "$0"
}

# List all functions that do not begin with an underscore _
_listAvailableFunctions() {
    cat $0 | grep -E '^[a-z]+[a-zA-Z0-9_]*\(\) \{$' | sed 's#() {$##' | sort
}

if [ $# -eq 0 ]
then
    _listAvailableFunctions
    exit
fi

"$@"
