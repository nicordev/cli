#! /usr/bin/env bash

readonly DUMMY_FILE="please_replace_test.initial.tmp"
readonly EXPECTED_FILE="please_replace_test.expected.tmp"

dummyMarkdown() {
    cat << 'EOF'
## veste

- [decathlon](https://www.decathlon.fr/browse/c0-tous-les-sports/c1-randonnee-trek/c4-chaussures-enfant/_/N-11hekzf)

## chaussures

- [decathlon](https://www.decathlon.fr/search?Ntt=veste%20imperm%C3%A9able%20enfant&facets=sizeValueLabel:7-8%20ANS_sportGroupLabels:randonn%C3%A9e_)
- [vivobarefoot](https://www.vivobarefoot.com/eu/kids/outdoor)
- [wildling shoes](https://www.wildling.shoes/en/collections/all?features=feature_wasserabweisend&tag=Kinder)
EOF
}

expectedResult() {
    cat << 'EOF'
veste

https://www.decathlon.fr/browse/c0-tous-les-sports/c1-randonnee-trek/c4-chaussures-enfant/_/N-11hekzf

chaussures

https://www.decathlon.fr/search?Ntt=veste%20imperm%C3%A9able%20enfant&facets=sizeValueLabel:7-8%20ANS_sportGroupLabels:randonn%C3%A9e_
https://www.vivobarefoot.com/eu/kids/outdoor
https://www.wildling.shoes/en/collections/all?features=feature_wasserabweisend&tag=Kinder
EOF
}

testRemoveMarkdownFromPaste() {
    dummyMarkdown | pbcopy
    please_markdown removeMarkdownFromPaste

    local result="$(pbpaste)"
    local expected="$(expectedResult)"

    if [ $debug ]
    then
        diff "$1" "$2"
        cat <<< $result

        return
    fi

    if [ "$(pbpaste)" != "$(expectedResult)" ]
    then
        echo 'failed'
        return
    fi

    echo 'success'
}

testRemoveMarkdownFromPaste
