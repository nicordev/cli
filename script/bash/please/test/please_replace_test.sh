#! /usr/bin/env bash

readonly TEMPORARY_TEST_DATA_DIRECTORY="temporary_test_data"
readonly TEMPORARY_TEST_DATA_DIRECTORY_INITIAL="$TEMPORARY_TEST_DATA_DIRECTORY/initial/HelloWorld"
readonly TEMPORARY_TEST_DATA_DIRECTORY_EXPECTED="$TEMPORARY_TEST_DATA_DIRECTORY/expected/ILikeOrange"
readonly DUMMY_FILE="$TEMPORARY_TEST_DATA_DIRECTORY_INITIAL/HelloWorld.tmp"
readonly EXPECTED_FILE="$TEMPORARY_TEST_DATA_DIRECTORY_EXPECTED/ILikeOrange.tmp"

_askConfirmationDefaultYes() {
    echo -e "${STYLE_BOLD}Continue?${STYLE_RESET} [YES/no] "
    read answer

    if [[ ${answer,,} =~ ^n ]]; then

        return 1
    fi

    return 0
}

makeTemporaryTestDataDirectory() {
    mkdir -p "$TEMPORARY_TEST_DATA_DIRECTORY_INITIAL"
    mkdir -p "$TEMPORARY_TEST_DATA_DIRECTORY_EXPECTED"
}

makeDummyFile() {
    cat << 'EOF' > $DUMMY_FILE
helloWorld
HelloWorld
'hello_world
'/{id}/%s/hello_world
'/{id}/hello_worlds/%s/hello_world
 hello world
EOF
}

makeExpectedFile() {
    cat << 'EOF' > $EXPECTED_FILE
iLikeOrange
ILikeOrange
'i_like_orange
'/{id}/%s/i_like_orange
'/{id}/i_like_oranges/%s/i_like_orange
 i like orange
EOF
}

clean() {
    echo "remove $TEMPORARY_TEST_DATA_DIRECTORY"
    _askConfirmationDefaultYes || return
    rm -rf "$TEMPORARY_TEST_DATA_DIRECTORY"
}

assertSameContent() {
    diff "$1" "$2" 1> /dev/null 2> /dev/null
    local result="$?"

    if [ $debug ]
    then
        diff "$1" "$2"
    fi

    return "$result"
}

assertSameString() {
    [ "$1" = "$2" ]
}

assertFileExists() {
    [ -f "$*" ]
}

testEverywhere() {
    local result=0

    debug= files="$DUMMY_FILE" please_replace everywhere HelloWorld ILikeOrange
    assertFileExists "$TEMPORARY_TEST_DATA_DIRECTORY/initial/ILikeOrange/ILikeOrange.tmp" || result=1
    assertSameContent $EXPECTED_FILE "$TEMPORARY_TEST_DATA_DIRECTORY/initial/ILikeOrange/ILikeOrange.tmp" || result=1

    if [ $result -gt 0 ]
    then
        echo 'failed'
        return
    fi

    echo -e 'success'
}

makeTemporaryTestDataDirectory
makeDummyFile
makeExpectedFile
echo "temporary data created."
_askConfirmationDefaultYes
testEverywhere

if [ ! "$debug" ]
then
    clean
fi
