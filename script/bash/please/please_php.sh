#! /bin/bash

readonly SCRIPT_NAME=$(basename $0)
readonly SCRIPT_DIRECTORY=.

run() {
    if [ $# -lt 2 ]
    then
        echo -e "
${SCRIPT_NAME} ${FUNCNAME[0]} \033[33mphpTag command\033[0m
"

        return 1
    fi

    local phpTag="$1"
    shift

    set -x
    docker run -it --rm -w /app -v $(pwd):/app php:${phpTag} $*
}

pest() {
    if [ $# -lt 2 ]
    then
        echo -e "
${SCRIPT_NAME} ${FUNCNAME[0]} \033[33mtestDirectory xdebugClientPort\033[0m
"
    fi

    local TEST_DIR="$1"
    local XDEBUG_CLIENT_PORT="$2"

    docker run --rm -it -e "XDEBUG_CONFIG=client_port=${XDEBUG_CLIENT_PORT:-9003}" -v "$(pwd):/app" --add-host host.docker.internal:host-gateway "${DOCKER_IMAGE:-my/pest}" pest "${TEST_DIR:-tests}" --testdox
}

# Display the source code of this file
howItWorks() {
    if [ $# -lt 1 ]
    then
        less "$0"

        return
    fi

    less --pattern="$@" "$0"
}

# List all functions that do not begin with an underscore _
_listAvailableFunctions() {
    cat $0 | grep -E '^[a-z]+[a-zA-Z0-9_]*\(\) \{$' | sed 's#() {$##' | sort
}

if [ $# -eq 0 ]
then
    _listAvailableFunctions
    exit
fi

"$@"
