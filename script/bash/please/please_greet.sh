#! /usr/bin/env bash

readonly SCRIPT_NAME=$(basename $0)
readonly SCRIPT_DIRECTORY=$(dirname $0)

# style
readonly STYLE_RESET=$(tput sgr0)
readonly STYLE_REVERSE=$(tput rev)
readonly STYLE_BOLD=$(tput bold)

# foreground
readonly FG_BLACK=$(tput setaf 0)
readonly FG_RED=$(tput setaf 1)
readonly FG_GREEN=$(tput setaf 2)
readonly FG_YELLOW=$(tput setaf 3)
readonly FG_BLUE=$(tput setaf 4)
readonly FG_MAGENTA=$(tput setaf 5)
readonly FG_CYAN=$(tput setaf 6)
readonly FG_WHITE=$(tput setaf 7)

# background
readonly BG_BLACK=$(tput setab 0)
readonly BG_RED=$(tput setab 1)
readonly BG_GREEN=$(tput setab 2)
readonly BG_YELLOW=$(tput setab 3)
readonly BG_BLUE=$(tput setab 4)
readonly BG_MAGENTA=$(tput setab 5)
readonly BG_CYAN=$(tput setab 6)
readonly BG_WHITE=$(tput setab 7)

greet() {
    if [ $# -lt 1 ]; then
        echo -e "${SCRIPT_NAME} ${FG_YELLOW}name [lang=fr]${STYLE_RESET}"

        return 1
    fi

    local name="$1"
    local lang="${2:-fr}"

    if [ $lang = "fr" ]
    then
        echo "Bonjour $name,"
        return
    fi

    if [ $lang = "en" ]
    then
        echo "Hello $name,"
        return
    fi

    echo "unknown lang"
}

greet "$@"
