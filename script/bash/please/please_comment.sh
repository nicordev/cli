#! /usr/bin/env bash

readonly SCRIPT_NAME=$(basename $0)
readonly SCRIPT_DIRECTORY=$(dirname $0)

# style
readonly STYLE_RESET=$(tput sgr0)
readonly STYLE_REVERSE=$(tput rev)
readonly STYLE_BOLD=$(tput bold)

# foreground
readonly FG_BLACK=$(tput setaf 0)
readonly FG_RED=$(tput setaf 1)
readonly FG_GREEN=$(tput setaf 2)
readonly FG_YELLOW=$(tput setaf 3)
readonly FG_BLUE=$(tput setaf 4)
readonly FG_MAGENTA=$(tput setaf 5)
readonly FG_CYAN=$(tput setaf 6)
readonly FG_WHITE=$(tput setaf 7)

# background
readonly BG_BLACK=$(tput setab 0)
readonly BG_RED=$(tput setab 1)
readonly BG_GREEN=$(tput setab 2)
readonly BG_YELLOW=$(tput setab 3)
readonly BG_BLUE=$(tput setab 4)
readonly BG_MAGENTA=$(tput setab 5)
readonly BG_CYAN=$(tput setab 6)
readonly BG_WHITE=$(tput setab 7)

help() {
    printf "
# Conventional comments
https://conventionalcomments.org/

# label

nitpick
praise
question
suggestion
thought

${SCRIPT_NAME} ${action:-copy} ${FG_YELLOW}label [isNonBlocking=0] [comment]${STYLE_RESET}
"
}

show() {
    if [ $# -lt 1 ]
    then
        action="${FUNCNAME[0]}"
        help

        return 1
    fi

    local label="$1"
    shift

    case "${label}" in
        'n'|'nitpick')
            label='nitpick'
        ;;
        'p'|'praise')
            label='praise'
        ;;
        'q'|'question')
            label='question'
        ;;
        's'|'suggestion')
            label='suggestion'
        ;;
        't'|'thought')
            label='thought'
        ;;
        *)
            return
        ;;
    esac

    local isBlocking="$1"

    if [ "$isBlocking" != '1' ] && [ "$isBlocking" != '0' ]
    then
        isBlocking=0
    else 
        shift
    fi

    local comment="$*"
    local decoration=''

    if [ "$isBlocking" -eq 0 ]
    then
        local decoration=' (non-blocking)'
    fi

    printf "**$label$decoration:** $comment"
}

copy() {
    if [ $# -lt 1 ]
    then
        action="${FUNCNAME[0]}"
        help

        return 1
    fi

    if [[ "$OSTYPE" == "darwin"* ]]
    then
        # Mac OSX
        readonly COPY='pbcopy'
    elif [[ "$OSTYPE" == "linux-gnu"* ]]
    then
        # Linux
        readonly COPY='xclip -in -selection clipboard'
    else
        # Unknown.
        echo "Your OS is not supported for this operation."

        return 1
    fi

    show $* | ${PLEASE_COPY:-${COPY}}
}

nitpick() {
    copy nitpick $*
}

praise() {
    local comment="${*:-well done!}"

    copy praise 0 $comment
}

question() {
    copy question $*
}

suggestion() {
    copy suggestion $*
}

thought() {
    copy thought 0 $*
}

# Display the source code of this file
howItWorks() {
    if [ $# -lt 1 ]
    then
        less "$0"

        return
    fi

    less --pattern="$@" "$0"
}

# List all functions that do not begin with an underscore _
_listAvailableFunctions() {
    cat $0 | grep -E '^[a-z]+[a-zA-Z0-9_]*\(\) \{$' | sed 's#() {$##' | sort
}

if [ $# -eq 0 ]
then
    _listAvailableFunctions
    exit
fi

"$@"
