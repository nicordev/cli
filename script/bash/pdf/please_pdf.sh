#! /bin/bash

SCRIPT_NAME=$(basename $BASH_SOURCE)
SCRIPT_DIRECTORY=$(dirname $BASH_SOURCE)

_askConfirmationDefaultYes() {
    echo -e "\e[1mContinue?\e[0m [YES/no] "
    read answer

    if [[ ${answer,,} =~ ^n ]]; then
        return 1
    fi

    return 0
}

replacePage() {
    local readonly DEFAULT_OUTPUT="replaced.pdf"

    if [ $# -lt 3 ]; then
        echo -e "
\e[34moutput=$DEFAULT_OUTPUT\e[0m ${SCRIPT_NAME} ${FUNCNAME[0]} \e[33mdocument pageNumber pageFile\e[0m
"

        return 1
    fi

    local readonly DOCUMENT="$1"
    local readonly PAGE_NUMBER="$2"
    local readonly PAGE_FILE="$3"
    local readonly FINAL_DOCUMENT=${output:-$DEFAULT_OUTPUT}

    echo "Will insert \e[1m$PAGE_FILE\e[0m at page \e[1m$PAGE_NUMBER\e[0m from \e[1m$DOCUMENT\e[0m to \e[1m$FINAL_DOCUMENT\e[0m"
    _askConfirmationDefaultYes || return

    # split pages
    pdftk $DOCUMENT burst
    
    # select pages to keep
    local i=0
    local pages=''

    for file in pg_*.pdf
    do 
        ((++i))

        if [ $i -eq $PAGE_NUMBER ]
        then
            pages="$pages $PAGE_FILE"
            continue
        fi

        pages="$pages $file"
    done

    # create final document
    pdftk $pages cat output $FINAL_DOCUMENT

    # remove temporary pages
    rm pg_*.pdf

    echo -e "
\e[32m$FINAL_DOCUMENT created\e[0m
"
}

# Display the source code of this file
howItWorks() {
    cat $0
}

# List all functions that do not begin with an underscore _
_listAvailableFunctions() {
    cat $0 | grep -E '^[a-z]+[a-zA-Z0-9_]*\(\) \{$' | sed 's#() {$##' | sort
}

if [ $# -eq 0 ]; then
    _listAvailableFunctions
    exit
fi

"$@"
