#! /bin/bash

# style
readonly STYLE_RESET=$(tput sgr0)
readonly STYLE_REVERSE=$(tput rev)
readonly STYLE_BOLD=$(tput bold)

# foreground
readonly FG_BLACK=$(tput setaf 0)
readonly FG_RED=$(tput setaf 1)
readonly FG_GREEN=$(tput setaf 2)
readonly FG_YELLOW=$(tput setaf 3)
readonly FG_BLUE=$(tput setaf 4)
readonly FG_MAGENTA=$(tput setaf 5)
readonly FG_CYAN=$(tput setaf 6)
readonly FG_WHITE=$(tput setaf 7)

# background
readonly BG_BLACK=$(tput setab 0)
readonly BG_RED=$(tput setab 1)
readonly BG_GREEN=$(tput setab 2)
readonly BG_YELLOW=$(tput setab 3)
readonly BG_BLUE=$(tput setab 4)
readonly BG_MAGENTA=$(tput setab 5)
readonly BG_CYAN=$(tput setab 6)
readonly BG_WHITE=$(tput setab 7)

suffix=$(id -un) # Get current user name

if [ -n "$1" ]; then
    suffix="$1"
fi

createExecutableFile() {
    touch "$1"
    chmod +x "$1"
}

writeScriptTemplate() {
    cat > "$1" << END_OF_TEMPLATE
#! /usr/bin/env bash

readonly SCRIPT_NAME=\$(basename \$0)
readonly SCRIPT_DIRECTORY=\$(dirname \$0)

# style
readonly STYLE_RESET=\$(tput sgr0)
readonly STYLE_REVERSE=\$(tput rev)
readonly STYLE_BOLD=\$(tput bold)

# foreground
readonly FG_BLACK=\$(tput setaf 0)
readonly FG_RED=\$(tput setaf 1)
readonly FG_GREEN=\$(tput setaf 2)
readonly FG_YELLOW=\$(tput setaf 3)
readonly FG_BLUE=\$(tput setaf 4)
readonly FG_MAGENTA=\$(tput setaf 5)
readonly FG_CYAN=\$(tput setaf 6)
readonly FG_WHITE=\$(tput setaf 7)

# background
readonly BG_BLACK=\$(tput setab 0)
readonly BG_RED=\$(tput setab 1)
readonly BG_GREEN=\$(tput setab 2)
readonly BG_YELLOW=\$(tput setab 3)
readonly BG_BLUE=\$(tput setab 4)
readonly BG_MAGENTA=\$(tput setab 5)
readonly BG_CYAN=\$(tput setab 6)
readonly BG_WHITE=\$(tput setab 7)

functionName() {
    if [ \$# -lt 1 ]
    then
        cat << EOF

\${SCRIPT_NAME} \${FUNCNAME[0]} \${FG_YELLOW}parameterName\${STYLE_RESET}

EOF

        return 0
    fi

    printf "
\${BG_GREEN}\${FG_WHITE}  Success  \${STYLE_RESET}
\n"
}

# Display the source code of this file
howItWorks() {
    if [ \$# -lt 1 ]
    then
        less "\$0"

        return 0
    fi

    less --pattern="\$@" "\$0"
}

# List all functions that do not begin with an underscore _
_listAvailableFunctions() {
    cat \$0 | grep -E '^[a-z]+[a-zA-Z0-9_]*\(\) \{$' | sed 's#() {\$##' | sort
}

if [ \$# -eq 0 ]
then
    _listAvailableFunctions
    exit
fi

"\$@"
END_OF_TEMPLATE
}

fileName="please_${suffix}.sh"

if [ -f "$fileName" ]; then
    printf "
${BG_RED}  $fileName already exists.  ${STYLE_RESET}

${STYLE_BOLD}Please remove it first:${STYLE_RESET}
rm $fileName
"
    exit 1
fi

printf "
Creating script ${fileName}...
"
createExecutableFile "$fileName"
writeScriptTemplate "$fileName"
printf "
${BG_GREEN}${FG_WHITE}  Done. Enjoy :)  ${STYLE_RESET}
\n"
