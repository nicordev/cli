#! /usr/bin/env bash

readonly SCRIPT_NAME=$(basename $0)
readonly MAX_COUNT="${MAX_COUNT:-15}"
readonly BOOKMARKS_FILE="$HOME/please_git_bookmarks"

styleNormal="\033[0m"
styleStrong="\033[1m"
styleWarning="\033[33m"
styleInfo="\033[34m"

_do_list_repositories() {
    repositories=${repositories:-""}

    if [ $# -eq 0 ]; then
        echo -e "${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}directory${styleNormal}"

        return
    fi

    local current_dir="$1"

    for dir in $current_dir/*
    do
        if [ ! -d "$dir" ]
        then
            continue
        fi

        if [ -d "$dir/.git" ]
        then
            repositories+="$dir "
        fi

        _do_list_repositories "$dir"
    done
}

list_repositories() {
    repositories=${repositories:-""}

    if [ $# -eq 0 ]; then
        echo -e "${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}directory${styleNormal}"

        return
    fi

    local current_dir="$1"

    _do_list_repositories "$current_dir"

    printf "%s\n" $repositories
}

list_repositories_urls() {
    if [ $# -eq 0 ]; then
        echo -e "${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}directory${styleNormal}"

        return
    fi

    local directory="$1"
    repositories="$(list_repositories $directory)"

    for repo in $repositories
    do
        (
            cd $repo && \
            git remote get-url origin
        )
    done
}

pull_branch_for_repositories_of_directory() {
    if [ $# -eq 0 ]; then
        echo -e "${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}branch directory${styleNormal}"
        return
    fi

    local branch="$1"
    shift
    local root_dir="$1"
    shift

    for directory in $(list_repositories $root_dir)
    do
        echo -e "\n${styleInfo}  pulling [$branch] in [$directory]  ${styleNormal}\n"
        (
            cd "$directory"
            git checkout "$branch" \
            && git pull origin "$branch" || echo -e "\n${styleWarning}  cannot pull [$branch] in [$directory]  ${styleNormal}\n"
        )
    done
}

filterBranches() {
    if [ $# -eq 0 ]; then
        echo -e "${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}criteriaHere${styleNormal}"
        return
    fi

    listBranchNames | grep "$1"
}

filterAllBranches() {
    git --no-pager branch --all | grep "$1" | sed 's#^* ##' | sed 's#  ##' | sed 's#remotes/origin/##' | sed 's/HEAD ->//g' | sort | uniq
}

switchBranch() {
    echo -e "\n${styleStrong}Switching to $1...${styleNormal}"
    git switch "$1"
}

selectBranch() {
    local hint="$*"

    echo -e "
${styleStrong}Active branch: ${styleNormal}$(showCurrentBranch)

${styleStrong}Available branches :${styleNormal}"

    local availableBranches=$(filterAllBranches "$hint")
    PS3="Which number? "

    select branchToSwitch in $availableBranches
    do
        switchBranch "$branchToSwitch"
        return
    done
}

_askConfirmationDefaultYes() {
    local answer='yes'
    echo -e "\e[1m$1Continue?${styleNormal} [YES/no] "
    read answer

    if [[ ${answer,,} =~ ^(n) ]]; then
        return 1
    fi
}

# Checkout or fetch then checkout if branch not found.
checkout() {
    if [ $# -lt 1 ]; then
        echo -e "${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}branchName${styleNormal}"
        return
    fi

    git checkout "$@" || (git fetch origin && git checkout "$@")
}

checkoutPreviousBranch() {
    if [ $# -lt 1 ]; then
        echo -e "
${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}number${styleNormal}

default: 1
"

        _askConfirmationDefaultYes || return
    fi

    declare -i number="${1:-1}"

    git checkout "@{-$number}"
}

changeGitHubToGitlabOrigin() {
    if [ $# -lt 1 ]; then
        echo -e "${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}projectName [branchName]${styleNormal}"
        exit
    fi

    git remote rename origin origin-github
    git remote add origin "https://gitlab.com/nicordev/${1}.git"
    git push --set-upstream origin ${2:-master}
}

createBranchFrom() {
    if [ $# -lt 2 ]
    then
        echo
        git --no-pager branch | grep ${hint:-''}
        echo "
${FG_BLUE}hint=''${STYLE_RESET} ${SCRIPT_NAME} ${FUNCNAME[0]} ${FG_YELLOW}sourceBranch branchName${STYLE_RESET}
"

        return 1
    fi

    local sourceBranch="$1"
    local branchName="$2"

    git checkout $sourceBranch
    git pull origin $sourceBranch
    git checkout -b "$branchName"
}

createGitlabMergeRequest() {
    local remote=${remote:-origin}

    if [ $# -lt 2 ]; then
        echo -e "
\e[35mremote=$remote branchToPush=$branchToPush${styleNormal} ${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}targetBranch title [description]${styleNormal}
"
        return
    fi

    git push \
        -o merge_request.create \
        -o merge_request.remove_source_branch \
        -o merge_request.target="$1" \
        -o merge_request.title="$2" \
        -o merge_request.description="$3" \
        $remote ${branchToPush:-$(showCurrentBranch)}
}

showCurrentBranch() {
    git branch --show-current
}

showOtherBranches() {
    git branch | grep --invert-match \* | sed 's#  ##'
}

listBranchNames() {
    git branch --format='%(refname:short)'
}

listPreviousBranches() {
    git reflog | grep 'moving from' | sed -r 's#.*moving from (.*) to (.*)#from \1 to \2#g' | head -n ${*:-${count:-7}}
}

deleteBranches() {
    if [ $# -eq 0 ]; then
        echo -e "${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}criteriaHere${styleNormal}"
        exit
    fi

    filterBranches "$1"
    _askConfirmationDefaultYes || exit
    git branch -D $(filterBranches "$1")
}

deleteAllBranchesBut() {
    if [ $# -lt 1 ]
    then
        echo -e "${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}branchToKeep [...branchToKeep]${styleNormal}"
        return
    fi

    _askConfirmationDefaultYes || exit

    for branch in $(listBranchNames)
    do
        # skip branches to keep
        for branchToKeep in $@
        do
            if [ "$branch" = "$branchToKeep" ]
            then
                continue 2
            fi
        done

        git branch -D $branch
    done
}

recreateBranchFrom() {
    if [ $# -lt 2 ]
    then
        git --no-pager branch
        echo -e "
${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}branchToRecreateHere rootBranchHere${styleNormal}
"
        exit
    fi

    local branchToRecreate="$1"
    local temporaryBranchToRecreate="inital_$branchToRecreate"
    local rootBranch="$2"

    echo -e "\e[1m# Remove potential remaining temporary branch ${styleWarning}$temporaryBranchToRecreate${styleNormal}"
    git branch -D "$temporaryBranchToRecreate"

    echo -e "\e[1m# Rename ${styleWarning}$branchToRecreate${styleNormal}\e[1m to ${styleWarning}$temporaryBranchToRecreate${styleNormal}"
    git checkout "$branchToRecreate"
    git branch -m "$temporaryBranchToRecreate"

    echo -e "\e[1m# Select ${styleWarning}$rootBranch${styleNormal}"
    git checkout "$rootBranch"

    echo -e "\e[1m# Recreate ${styleWarning}$branchToRecreate${styleNormal}"
    git checkout -b "$branchToRecreate"

    echo -e "\e[1m# Pick commits from ${styleWarning}$temporaryBranchToRecreate${styleNormal}"
    _askConfirmationDefaultYes || echo 'See ya!' && exit

    git cherry-pick "..$temporaryBranchToRecreate"
    git branch -D "$temporaryBranchToRecreate"
}

renameBranch() {
    if [ $# -lt 1 ]
    then
        git --no-pager branch
        echo -e "
${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}branchToRename newBranchName${styleNormal}
"
        return
    fi

    local initialName="$1"
    local newName="$2"

    echo "rename $1 to $2"

    _askConfirmationDefaultYes || return

    git checkout "$initialName"
    git branch -m "$newName"
}

executeOnBranch() {
    if [ $# -lt 2 ]
    then
        git --no-pager branch
        echo -e "
${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}branch command${styleNormal}
"
        return
    fi

    local branch="$1"
    shift

    git checkout "$branch" && \
    $*
}

executeOnBranches() {
    if [ $# -lt 2 ]
    then
        git --no-pager branch
        echo -e "
${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}command branch [branch]${styleNormal}
"
        return
    fi

    local command="$1"
    shift

    for branch in $*
    do
        executeOnBranch "$branch" $command
    done
}

fetchOrigin() {
    git fetch origin
}

resetCurrentBranchToOrigin() {
    local readonly CURRENT_BRANCH=$(showCurrentBranch)

    fetchOrigin
    git reset --hard "origin/$CURRENT_BRANCH"
}

backupCurrentBranch() {
    local currentBranch=$(showCurrentBranch)

    git checkout -b "backup_${currentBranch}"
    git checkout ${currentBranch}
}

checkoutLastBranch() {
    git checkout -
}

pullThenRebaseBranch() {
    if [ $# -lt 1 ]; then
        git branch | grep "$HINT"
        printf "
\e[34mHINT=${styleNormal} ${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}branchToPullHere${styleNormal}
"
        return
    fi

    local branchToPull="$1"

    checkout "$branchToPull" &&
    git pull origin "$branchToPull" &&
    checkoutLastBranch &&
    git rebase "$branchToPull"
}

pullThenMergeBranch() {
    if [ $# -lt 1 ]; then
        git branch | grep "$HINT"
        printf "
\e[34mHINT=${styleNormal} ${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}branchToPullHere${styleNormal}
"
        return
    fi

    local branchToPull="$1"

    checkout "$branchToPull" &&
    git pull origin "$branchToPull" &&
    checkoutLastBranch &&
    git merge "$branchToPull"
}

pullBranchForMultipleProjects() {
    if [ $# -lt 1 ]; then
        printf "
${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}branch directory [directory]${styleNormal}
"
        return
    fi

    local branch="$1"
    shift

    for directory in $*
    do
        cd "$directory"
        $debug git pull ${origin:-origin} "$branch"
        cd -
    done
}

editBranchDescription() {
    git branch --edit-description "$@"
}

showBranchDescription() {
    local currentBranch="$1"

    if [ $# -eq 0 ]; then
        echo -e "${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}branchNameHere${styleNormal}\n"
        local currentBranch=$(showCurrentBranch)
    fi

    git config "branch.${currentBranch}.description"
}

showChangedFilesOfLastCommit() {
    git --no-pager diff --name-only HEAD..HEAD~1
}

showChangedFilesFromBranch() {
    if [ $# -lt 1 ]
    then\
        printf "
${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}branchHere${styleNormal}
"
        return
    fi

    git --no-pager diff --name-only $1
}

listLogs() {
    git log --oneline --graph --max-count=$MAX_COUNT
}

listChangedFiles() {
    if [ $# -lt 1 ]
    then\
        printf "
${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}commitShaHere${styleNormal}
"
        return
    fi

    git log --name-only --max-count=1 $*
}

removeCommit() {
    if [ $# -lt 1 ]; then
        listLogs
        printf "
\e[34mMAX_COUNT=$MAX_COUNT${styleNormal} ${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}commitShaHere${styleNormal}
"
        return
    fi

    set -x
    local initialBranch=$(git branch --show-current)
    local commitSha="$1"
    git checkout "${commitSha}~1" && \
    git checkout -B pleaseGitRemoveCommit && \
    git cherry-pick "$commitSha".."$initialBranch" && \
    git branch -mf "$initialBranch"
    git log --oneline --graph --max-count=$maxCount
    set +x
}

resetCurrentBranchAsRemote() {
    if [ $# -lt 1 ]; then
        printf "
${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}remote${styleNormal}
"
        return
    fi

    local remote="$1"
    local currentBranch=$(showCurrentBranch)

    git reset --hard "$remote/$currentBranch"
}

resetCurrentBranchAsOrigin() {
    resetCurrentBranchAsRemote origin
}

bookmarkCurrentBranch() {
    bookmarkBranch $(showCurrentBranch) "$@"
}

bookmarkBranch() {
    local comment=""
    if [ $# -lt 1 ]
    then
        printf "
${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}branchNameHere${styleNormal}
"
        return
    fi

    local branch="$1"
    shift

    if [ "$2" ]
    then
        comment=" # $@"
    fi

    echo -e "$branch$comment" >> "$BOOKMARKS_FILE"
}

listBookmarks() {
    sed "s/^/git checkout /" "$BOOKMARKS_FILE"
}

editBookmarks() {
    vim "$BOOKMARKS_FILE"
}

stashCommit() {
    if [ $# -lt 2 ]
    then
        listLogs
        printf "
\e[34mMAX_COUNT=$MAX_COUNT${styleNormal} ${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}commitShaHere stashMessage${styleNormal}
"
        return
    fi

    git status
    echo 'Continue?'

    local commitSha="$1"
    local stashMessage="$2"
    local initialBranch=$(git branch --show-current)
    _askConfirmationDefaultYes || return

    set -x
    git checkout "${commitSha}~1" && \
    git cherry-pick --no-commit "$commitSha" && \
    git stash push -m "$stashMessage" && \
    git checkout "$initialBranch"
    set +x
}

stashChanges() {
    if [ $# -lt 2 ]
    then
        printf "
${SCRIPT_NAME} ${FUNCNAME[0]} ${styleWarning}stashMessage file [file]${styleNormal}
"
        return
    fi

    local message="$1"
    shift
    local files="$*"

    git add $files
    git stash push -m "$message" $files
    echo "changes stashed with message: $message"
    git --no-pager stash show
    git stash apply
}

pushBranches() {
    for branch in $(git branch | awk '{ print $1 }'); do if [ "$branch" = '*' ]; then continue; fi; git checkout "$branch"; git push origin HEAD; done
}

# Display the source code of this file
howItWorks() {
    if [ $# -lt 1 ]
    then
        less "$0"

        return
    fi

    less --pattern="$@" "$0"
}

# List all functions that do not begin with an underscore _
_listAvailableFunctions() {
    cat $0 | grep -E '^[a-z]+[a-zA-Z0-9_]*\(\) \{$' | sed 's#() {$##' | sort
}

if [ $# -eq 0 ]; then
    _listAvailableFunctions
    exit
fi

"$@"
