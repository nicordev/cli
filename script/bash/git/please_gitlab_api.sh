#! /usr/bin/env bash

readonly SCRIPT_NAME=$(basename $0)
readonly SCRIPT_DIRECTORY=$(dirname $0)

# style
readonly STYLE_RESET=$(tput sgr0)
readonly STYLE_REVERSE=$(tput rev)
readonly STYLE_BOLD=$(tput bold)

# foreground
readonly FG_BLACK=$(tput setaf 0)
readonly FG_RED=$(tput setaf 1)
readonly FG_GREEN=$(tput setaf 2)
readonly FG_YELLOW=$(tput setaf 3)
readonly FG_BLUE=$(tput setaf 4)
readonly FG_MAGENTA=$(tput setaf 5)
readonly FG_CYAN=$(tput setaf 6)
readonly FG_WHITE=$(tput setaf 7)

# background
readonly BG_BLACK=$(tput setab 0)
readonly BG_RED=$(tput setab 1)
readonly BG_GREEN=$(tput setab 2)
readonly BG_YELLOW=$(tput setab 3)
readonly BG_BLUE=$(tput setab 4)
readonly BG_MAGENTA=$(tput setab 5)
readonly BG_CYAN=$(tput setab 6)
readonly BG_WHITE=$(tput setab 7)

# gitlab
readonly GITLAB_HOST="${gitlabHost:-https://gitlab.com/api/v4}"
accessToken="${accessToken:-${GITLAB_ACCESS_TOKEN}}"

_assertAccessToken() {
    if [ -z "${accessToken}" ]
    then
        printf "
${BG_RED}${FG_WHITE}  missing accessToken  ${STYLE_RESET}\n
"

        return 2
    fi
}

call() {
    if [ $# -lt 1 ]
    then
        printf "
accessToken='' ${SCRIPT_NAME} ${FUNCNAME[0]} ${FG_YELLOW}uri${STYLE_RESET}
"

        return
    fi

    local uri="$*"

    $debug curl --request GET --header "PRIVATE-TOKEN: $accessToken" --location "${GITLAB_HOST}${uri}" | jq
}

myProjects() {
    local queryParameters="$*"

    call "/projects?owned=true&$queryParameters"
}

myProjectsSshUrls() {
    local queryParameters="$*"

    myProjects "$queryParameters" | jq '.[].ssh_url_to_repo' | tr -d '"'
}

# Display the source code of this file
howItWorks() {
    if [ $# -lt 1 ]
    then
        less "$0"

        return
    fi

    less --pattern="$@" "$0"
}

# List all functions that do not begin with an underscore _
_listAvailableFunctions() {
    cat $0 | grep -E '^[a-z]+[a-zA-Z0-9_]*\(\) \{$' | sed 's#() {$##' | sort
}

if [ $# -eq 0 ]
then
    _listAvailableFunctions
    exit
fi

_assertAccessToken || exit $?

"$@"
