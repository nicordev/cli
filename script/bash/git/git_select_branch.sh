#!/bin/bash

styleStrong="\033[1m"
styleNormal="\033[0m"

showCurrentBranch() {
    git branch --show-current
}

filterBranches() {
    git --no-pager branch --all | grep "$1" | sed 's#^* ##' | sed 's#  ##' | sed 's#remotes/origin/##' | sed 's/HEAD ->//g' | sort | uniq
}

switchBranch() {
    echo -e "\n${styleStrong}Switching to $1...${styleNormal}"
    git switch "$1"
}

selectBranch() {
    local hint="$*"

    echo -e "
${styleStrong}Active branch: ${styleNormal}$(showCurrentBranch)

${styleStrong}Available branches :${styleNormal}"

    local availableBranches=$(filterBranches $hint)
    PS3="Which number? "

    select branchToSwitch in $availableBranches
    do
        switchBranch "$branchToSwitch"
        return
    done
}

selectBranch
