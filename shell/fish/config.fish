if status is-interactive
    # Commands to run in interactive sessions can go here
end

function fish_prompt
    function parse_git_branch
        set_color --bold yellow
        git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
        set_color normal
    end

    set_color 5188a5
    echo (date "+%Y-%m-%d") fish
    echo  (pwd) (parse_git_branch) (set_color normal)
    echo
end
