#!/usr/bin/env bash

echo "Linking aliases to $HOME..."
ln -f aliases/* $HOME/aliases
