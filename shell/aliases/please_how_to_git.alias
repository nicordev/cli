# git
alias please_git_how_to_ignore_globally='echo -e "
# create a .gitignore file in home directory
touch ~/.gitignore

# run 
git config --global core.excludesfile ~/.gitignore
"'
alias please_git_how_to_set_ssh_url='echo "
git remote set-url \e[33moriginNameHere\e[0m git@\e[33mhostHere\e[0m:\e[33musernameHere\e[0m/\e[33mprojectNameHere\e[0m.git

# for github:
git remote set-url origin-github git@github.com:nicordev/learn_docker.git

# for gitlab:
git remote set-url origin git@gitlab.com:nicordev/learn_docker.git
"'
alias please_git_how_to_show_static_result='echo "
git --no-pager \e[33msomeOtherCommand\e[0m
git -P \e[33msomeOtherCommand\e[0m
"'
alias please_git_how_to_switch_to_previous_branch='echo "git checkout -"'
alias please_git_how_to_rebase_some_branches="echo \"
for branch in \\\$(please_git listBranchNames); do echo rebasing \\\$branch; git rebase \\\$branch; done | grep --invert-match -E '${FG_YELLOW}regexHere${STYLE_RESET}'
\""
alias please_git_how_to_squash_commits="printf \"
To squash commits in the current branch:

git rebase -i \e[33mcommitShaHere\e[0m
git rebase --interactive \e[33mcommitShaHere\e[0m

in the text editor, replace \e[1mpick\e[0m by \e[1msquash\e[0m for the wanted commits
save then exit
enter a commit message
save then exit
\""
alias please_git_how_to_show_next_commits="printf \"
git log \e[33mcommitShaHere\e[0m~1..HEAD
\""
alias please_git_how_to_use_git_log="printf \"
# show some commits in one line with graph:
git log --oneline --graph --max-count=\e[33mcommitsCountToShowHere\e[0m

# show commits in a range:
git log \e[33mstartCommitShaHere\e[0m~1..\e[33mendCommitShaHere\e[0m

# show commits after a specific one:
git log \e[33mcommitShaHere\e[0m~1..HEAD

# remove infos
git log --no-decorate --oneline

# show changed files
git log --name-only

# search a string in commit message
git log --grep='my string here'
\""
please_git_how_to_find_a_commit_that_contains_a_particular_change() {
    cat << 'EOF'
git log -S"some change"
git log -S"some regex" --pickaxe-regex
git log -G"some regex"
EOF
}
alias please_git_how_to_show_current_branch='echo -e "git branch --show-current
git symbolic-ref --short HEAD"'
alias please_git_how_to_edit_branch_description='echo -e "git branch --edit-description"'
alias please_git_how_to_show_branch_description='echo -e "git config branch.\e[33mbranchNameHere\e[0m.description"'
alias please_git_how_to_add_note_to_commit='echo "git note add"'
alias please_git_how_to_show_stash_modifications='echo -e "git stash show -p stash@{0}"'
alias please_git_how_to_stash_unstaged_changes='echo "git stash --include-untracked"'
alias please_git_how_to_remove_untracked_files='echo "git clean -fd"'
alias please_git_how_to_modify_last_commit_without_editing_commit_message='echo "git commit --amend --no-edit"'
alias please_git_how_to_push_with_options='echo -e "git push -o ci.variable=\"\e[33mvariableName=variableValue\"\e[0m -o ci.variable=\"\e[33mvariableName=variableValue\"\e[0m origin HEAD"'
alias please_git_how_to_show_staged_changes='echo "git diff --staged"'
alias please_git_how_to_show_changes='echo "
git diff HEAD
git status -vv
"'
alias please_git_how_to_show_unstaged_changes='echo "git diff HEAD"'
alias please_git_how_to_clone_from_github='echo -e "git clone https://github.com/\e[33muserName\e[0m/\e[33mrepositoryName\e[0m.git"'
alias please_git_how_to_push_repository_to_github='echo -e "git remote add origin  https://github.com/\e[33muserName\e[0m/\e[33mrepositoryName\e[0m.git\ngit push --set-upstream \e[33morigin master\e[0m"'
alias please_git_how_to_create_remote="printf \"
- in the remote server directory:

git init --bare

- in our local project directory:

git remote add remoteNameHere remoteInfoHere

where remoteInfo can be:
- remote server url
- userNameHere@remoteServerHostHere:remoteServerDirectory
\""
alias please_git_how_to_show_a_graphic_interface='echo "gitg"'
alias please_git_how_to_cherry_pick='echo -e "
# Bring commits of a branch to the active branch from a specific commit:
git cherry-pick \e[33mcommitSha\e[0m^..\e[33mbranchName\e[0m

# Bring last commits of a branch to the active branch:
git cherry-pick ..\e[33mbranchName\e[0m

# Bring only the last commit of a branch to the active branch:
git cherry-pick \e[33mbranchName\e[0m

# Bring commit changes without creating a commit:
git cherry-pick --no-commit \e[33mcommitShaHere\e[0m
"'
alias please_git_how_to_hook="printf \"
git documentation:
https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
https://www.git-scm.com/docs/githooks

create an executable file in your project's .git/hooks directory named like this:

.git/hooks/\e[33mhookNameHere\e[0m

where \e[33mhookNameHere\e[0m can be:

pre-commit
prepare-commit-msg
commit-msg
post-commit
pre-rebase
post-rewrite
post-checkout
post-merge
pre-push
pre-auto-gc
\""
alias please_git_how_to_list_branches_without_asterisk='echo "
git branch --format=\"%(refname:short)\"
"'
alias please_git_what_emoji_to_use='echo "
https://gitmoji.dev/

:art:           code style
:technologist:  improve developer experience
:sparkles:      add feature
:dizzy:         change existing feature
"'

please_git_show_gitmoji_list() {
    cat << EOF
 🎨   :art:                        Improving structure / format of the code.
 ⚡️   :zap:                        Improving performance.
 🔥   :fire:                       Removing code or files.
 🐛   :bug:                        Fixing a bug.
 🚑   :ambulance:                  Critical hotfix.
 ✨    :sparkles:                   Introducing new features.
 💫   :dizzy:                      Changing an existing feature.
 📝   :pencil:                     Writing docs.
 🚀   :rocket:                     Deploying stuff.
 💄   :lipstick:                   Updating the UI and style files.
 🎉   :tada:                       Initial commit.
 ✅    :white_check_mark:           Updating tests.
 🔒   :lock:                       Fixing security issues.
 🍎   :apple:                      Fixing something on macOS.
 🐧   :penguin:                    Fixing something on Linux.
 🏁   :checkered_flag:             Fixing something on Windows.
 🤖   :robot:                      Fixing something on Android.
 🍏   :green_apple:                Fixing something on iOS.
 🔖   :bookmark:                   Releasing / Version tags.
 🚨   :rotating_light:             Removing linter warnings.
 🚧   :construction:               Work in progress.
 💚   :green_heart:                Fixing CI Build.
 ⬇️   :arrow_down:                 Downgrading dependencies.
 ⬆️   :arrow_up:                   Upgrading dependencies.
 📌   :pushpin:                    Pinning dependencies to specific versions.
 👷   :construction_worker:        Adding CI build system.
 📈   :chart_with_upwards_trend:   Adding analytics or tracking code.
 ♻️   :recycle:                    Refactoring code.
 🐳   :whale:                      Work about Docker.
 ➕    :heavy_plus_sign:            Adding a dependency.
 ➖    :heavy_minus_sign:           Removing a dependency.
 🔧   :wrench:                     Changing configuration files.
 🌐   :globe_with_meridians:       Internationalization and localization.
 ✏️   :pencil2:                    Fixing typos.
 💩   :poop:                       Writing bad code that needs to be improved.
 ⏪    :rewind:                     Reverting changes.
 🔀   :twisted_rightwards_arrows:  Merging branches.
 📦   :package:                    Updating compiled files or packages.
 👽   :alien:                      Updating code due to external API changes.
 🚚   :truck:                      Moving or renaming files.
 📄   :page_facing_up:             Adding or updating license.
 💥   :boom:                       Introducing breaking changes.
 🍱   :bento:                      Adding or updating assets.
 👌   :ok_hand:                    Updating code due to code review changes.
 ♿️   :wheelchair:                 Improving accessibility.
 💡   :bulb:                       Documenting source code.
 🍻   :beers:                      Writing code drunkenly.
 💬   :speech_balloon:             Updating text and literals.
 🗃   :card_file_box:              Performing database related changes.
 🔊   :loud_sound:                 Adding logs.
 🔇   :mute:                       Removing logs.
 👥   :busts_in_silhouette:        Adding contributor(s).
 🚸   :children_crossing:          Improving user experience / usability.
 🏗   :building_construction:      Making architectural changes.
 📱   :iphone:                     Working on responsive design.
 🤡   :clown_face:                 Mocking things.
 🥚   :egg:                        Adding an easter egg.
 🙈   :see_no_evil:                Adding or updating a .gitignore file
 📸   :camera_flash:               Adding or updating snapshots
 ⚗    :alembic:                    Experimenting new things
 🔍   :mag:                        Improving SEO
 ☸️   :wheel_of_dharma:            Work about Kubernetes
 🏷️  :label:                      Adding or updating types (Flow, TypeScript)
EOF
}

alias please_git_how_to_compare_files='echo -e "
git diff ${FG_YELLOW}branchOrCommitName${STYLE_RESET}:${FG_YELLOW}pathTo/file pathTo/other/file${STYLE_RESET}

# diff with staged changes
git diff --staged ${FG_YELLOW}branchOrCommitName${STYLE_RESET}:${FG_YELLOW}pathTo/file pathTo/other/file${STYLE_RESET}
git diff --cached ${FG_YELLOW}branchOrCommitName${STYLE_RESET}:${FG_YELLOW}pathTo/file pathTo/other/file${STYLE_RESET}
"'
alias please_git_how_to_create_patch='echo -e "
# using git diff
git diff > ${FG_YELLOW}my_patch.txt${STYLE_RESET}

## on some files
git diff ${FG_YELLOW}branchOrCommitName${STYLE_RESET}:${FG_YELLOW}pathTo/file${STYLE_RESET} -- ${FG_YELLOW}pathTo/other/file pathTo/another/file${STYLE_RESET} > ${FG_YELLOW}my_patch.txt${STYLE_RESET}

# using git format-patch

## on some files
git format-patch ${FG_YELLOW}branchOrCommitName${STYLE_RESET}:${FG_YELLOW}pathTo/file${STYLE_RESET} -- ${FG_YELLOW}pathTo/other/file pathTo/another/file${STYLE_RESET}

## for 1 commit
git format-patch -1 ${FG_YELLOW}commitSha${STYLE_RESET}
"'
alias please_git_how_to_apply_patch='echo "
git apply my_patch.txt
"'
alias please_git_how_to_revert_a_commit='echo -e "
git revert ${FG_YELLOW}commitSha${STYLE_RESET}
"'

please_git_how_to_list_changed_files() {
    cat << EOF
# one commit
git log --name-only --max-count=1 ${FG_YELLOW}commitShaHere${STYLE_RESET}

# multiple commits
git diff --name-only ${FG_YELLOW}commitShaHere${STYLE_RESET}..${FG_YELLOW}commitShaHere${STYLE_RESET}

# last commit
git diff --name-only HEAD..HEAD~1

# compare current branch with another branch
git diff --name-only ${FG_YELLOW}otherBranchName${STYLE_RESET}
EOF
}

please_git_how_to_list_untracked_files_only() {
    cat << 'EOF'
git status -u | grep -E '^\t[^ ]+$' | tr -d "\t"
EOF
}

please_git_how_to_commit_files_partially() {
    cat << 'EOF'
# enter interactive mode
git add -i
git add --interactive

# select hunks
5 patch to select hunks
    s to split the current hunk
    y to add the hunk
    n to avoid the hunk
7 quit

# then commit
git commit
EOF
}

please_gitlab_how_to_use_conventional_commit_extension() {
    cat << 'EOF'
# Clone this repo
git clone git@gitlab.com:conventionalcomments/conventional-comments-button.git

# On Firefox: enter into the address bar
about:debugging#/runtime/this-firefox

# In the Extension page: Load Temporary Add-on... and select any file within the cloned repository

https://gitlab.com/conventionalcomments/conventional-comments-button
EOF
}

please_git_how_to_find_bugs() {
    cat << 'EOF'
# 1. start
git bisect start

# 2. set boundaries
git bisect bad HEAD_or_another_commit_having_the_error
git bisect good lastGoodCommitShaHere

# 3. run the test and see if it fails
## if it fails:
git bisect bad
## if it pass:
git bisect good

# 4. do 3. as long as git asks it
# 5. the bad commit SHA found by git appear, copy it then reset
git bisect reset
EOF
}

please_git_how_to_pull() {
    cat << EOF
# without upstream branch
git pull ${FG_YELLOW}branchNameHere${STYLE_RESET}

# with upstream branch
git branch --set-upstream ${FG_YELLOW}branchNameHere${STYLE_RESET}
git pull

# if hint You have divergent branches and need to specify how to reconcile them.
git pull --rebase ${FG_YELLOW}branchNameHere${STYLE_RESET}
EOF
}

please_git_how_to_push_project() {
    cat << 'EOF'
# the project must have at least one commit
git remote add origin git@gitlab.com:nameSpaceHere/projectNameHere.git
git push --set-upstream origin master
EOF
}

please_git_how_to_rename_old_commit_message() {
    cat << 'EOF'
# given we are on branch A
# create a temporary branch
git checkout -b renamingBranch

# remove commits after the commit to rename
git reset --hard commitToRenameSha

# amend the commit to rename
git commit --amend

# change the message in the text editor then save

# bring back commits removed
git cherry-pick ..branchA

# go back to branch A
git checkout branchA

# remove the commit to rename and the commits that were created afterward
git reset --hard commitBeforeTheCommitToRenameSha

# bring the commits from the temporary branch
git cherry-pick ..renamingBranch

# remove the temporary branch
git branch -D renamingBranch
EOF
}

please_git_how_to_revert_changes_of_a_file() {
    cat << EOF
# using a commit
git checkout ${FG_YELLOW}commit_sha_to_revert${STYLE_RESET}~1 -- ${FG_YELLOW}file_to_revert${STYLE_RESET}

# using a branch
git checkout ${FG_YELLOW}branch_name${STYLE_RESET} -- ${FG_YELLOW}file_to_revert${STYLE_RESET}
EOF
}
